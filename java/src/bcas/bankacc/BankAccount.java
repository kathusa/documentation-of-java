package bcas.bankacc;
/**
 * To perform basic bank functions
 * @author KathuS
 *@since 17- 05- 2020 
 *@version 1.0.0v
 */

public class BankAccount {
	/**
	 * String branch, double accBalance,String accType,String accNumber,accHolderName
	 * 
	 */
	
	
		public static final String bankName = "BOC Bank";
		private String branch;
		private double accBalance = 0;
		private String accType;
		private String accNumber;
		private String accHolderName;
		
		/**
		 * to add theses items.
		 * @param branch get the detail 
		 * @param accName get the accName
		 * @param accNumber get accNumber
		 * @param accHolderName to get accHolderName
		 * @param amount to get the amount
		 */
		
		public void openAccount(String branch,String accName,String accNumber,String accHolderName,int amount) {
			
			this.branch = branch;
			this.accType = accName;
			this.accNumber = accNumber;
			this.accHolderName = accHolderName;
			this.accBalance = amount;
			
			/**
			 * to add accBalance and accType .
			 * 
			 */
			
		}
		
		public void withdrawal (double amount) {
			accBalance = accBalance + amount;
		/**
		 * to add accBalance+amount
		 */
			
		}
		public void deposit(double amount) {
			accBalance = accBalance + amount;
			/**
			 * to add accBalance+amount
			 */	
			
		}
		public double getAccBlance() {
			return accBalance;
			/**
			 * to  get accBalance 
			 * 
			 * return accBalance
			 */
			
		}
		public String getAccountHolderName() {
			return accHolderName;
			/**
			 *  to get AccountHolderName
			 *  
			 *  return AccHolderName
			 */
			
		}
		public void getAccountDetatils() {
			/**
			 * This is the main method of this program .
			 * 
			 */
			
			System.out.println("Bank Name : " + bankName + "-"+branch);
			System.out.println("Account Number : " + accNumber);
			System.out.println("Account Type : " + accType);
			System.out.println("Account Holder Name : " + accHolderName);
			System.out.println("Account Balance : " + accBalance);
			System.out.println ("................................");
		}
	}

