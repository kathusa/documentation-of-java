package java;;

/**
 * To perform basic calculator functions
 * 
 * @author KathuS
 *@since 17- 05- 2020 
 *@version 1.0.0v
 *
 */

public class simpleCalculater {
	
	/**
	 * variable a, number 2 value 
	 */
	public int a  ;
	/**
	 * /**
	 * variable a, number 2 value 
	 */
	public int b  ;
	
	/**
	 * to add integer numbers.
	 * will return sum given two integers(a+b)
	 * 
	 * @param a adding first value 
	 * @param b adding second value 
	 * @return (a+b)
	 */
	public int sum(int a ,int b) {
	 return (a + b);
	 }

	/**
	 * to add subtract two integer numbers.
	 * will return sum given two integers(a-b/b-a)
	 * 
	 * @param a subtraction first value
	 * @param b subtraction second value 
	 * @return (a-b)
	 */
	public int sub(int a ,int b) {
	 return (a - b);
	}
	/**
	 * This is the main method of this program
	 * @param args for input arguments
	 */
	
public static void main(String[]args) {
	simpleCalculater cal = new simpleCalculater();
	System.out.println(cal.sub(10, 20));
 }
}






